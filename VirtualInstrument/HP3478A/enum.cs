﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using System.ComponentModel;
using SUF.Instrument;

namespace SUF.Instrument.HP3478A
{ 
    public enum MeasurementFunction
    {
        [Command("F1"), Description("DC Volts")]
        DC_Volts,
        [Command("F2"), Description("AC Volts")]
        AC_Volts,
        [Command("F3"), Description("2-Wire Ohms")]
        Ohms_2wire,
        [Command("F4"), Description("4-Wire Ohms")]
        Ohms_4wire,
        [Command("F5"), Description("DC Current")]
        DC_Current,
        [Command("F6"), Description("AC Current")]
        AC_Current,
        [Command("F7"), Description("Extended Ohms")]
        Ohms_Extended
    }
    public enum Range
    {
        [Command("R-2"), Description("30mV")]
        DCV_30mV,
        [Command("R-1"), Description("300mV")]
        DCV_300mV,
        [Command("R-1"), Description("300mV")]
        ACV_300mV,
        [Command("R-1"), Description("300mA")]
        DCI_300mA,
        [Command("R-1"), Description("300mA")]
        ACI_300mA,
        [Command("R0"), Description("3V")]
        DCV_3V,
        [Command("R0"), Description("3V")]
        ACV_3V,
        [Command("R0"), Description("3A")]
        DCI_3A,
        [Command("R0"), Description("3A")]
        ACI_3A,
        [Command("R1"), Description("30V")]
        DCV_30V,
        [Command("R1"), Description("30V")]
        ACV_30V,
        [Command("R1"), Description("30ohm")]
        R_30ohm,
        [Command("R2"), Description("300V")]
        DCV_300V,
        [Command("R2"), Description("300V")]
        ACV_300V,
        [Command("R2"), Description("300ohm")]
        R_300ohm,
        [Command("R3"), Description("3Kohm")]
        R_3kohm,
        [Command("R4"), Description("30Kohm")]
        R_30kohm,
        [Command("R5"), Description("300Kohm")]
        R_300kohm,
        [Command("R6"), Description("3Mohm")]
        R_3Mohm,
        [Command("R7"), Description("30Mohm")]
        R_30Mohm,
        [Command("RA"), Description("Auto")]
        DCV_Auto,
        [Command("RA"), Description("Auto")]
        ACV_Auto,
        [Command("RA"), Description("Auto")]
        DCI_Auto,
        [Command("RA"), Description("Auto")]
        ACI_Auto,
        [Command("RA"), Description("Auto")]
        R_Auto
    }

    public enum Resolution
    {
        [Command("N3"), Description("3.5")]
        Digit_35,
        [Command("N4"), Description("4.5")]
        Digit_45,
        [Command("N5"), Description("5.5")]
        Digit_55,
    }

    public enum Trigger
    {
        [Command("T1")]
        Internal,
        [Command("T2")]
        External,
        [Command("T3")]
        Single,
        [Command("T4")]
        Hold,
        [Command("T5")]
        Fast
    }
}
