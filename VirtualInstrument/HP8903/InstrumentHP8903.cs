﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SUF.Instrument;

namespace SUF.Instrument.HP8903
{
    [PluginDescriptor("HP 8903B Audio Analyzer")]
    [PluginSettingsClass("SUF.Instrument.HP8903.FormHP8903InstrumentSettings")]
    public class InstrumentHP8903 : Instrument
    {
        public InstrumentHP8903() { }
        public AudioAnalyzerType analyzerType = AudioAnalyzerType.HP8903B;

        // Functions
        public bool enabled_AC = false;
        public bool enabled_DC = false;
        public bool enabled_Distortion = false;
        public bool enabled_DistortionLevel = false;
        public bool enabled_SNR = false;
        public bool enabled_SINAD = false;

        // Generator
        public ResultDescriptor frequencySourceDescriptor;
        public FrequencyUnits GeneratorFrequencyUnit = FrequencyUnits.kHz;
        public double GeneratorFrequency = 1;

        public ResultDescriptor amplitudeSourceDescriptor;
        public AmplitudeUnits GeneratorAmplitudeUnit = AmplitudeUnits.V;
        public double GeneratorAmplitude = 1;

        public SourceOutputImpedance GeneratorImpedance = SourceOutputImpedance.Ohm600;

        // AC Measurement
        public InputRangeAC AC_InputRange = InputRangeAC.Auto;
        public ScaleType AC_ScaleType = ScaleType.Lin;
        public bool AC_Ratio = false;
        public PostNotchGain AC_PostNotchGain = PostNotchGain.Auto;
        public DetectorType AC_DetectorType = DetectorType.RMS_Fast;
        public bool AC_HoldNotchTuning = false;
        public bool AC_PowerDisplay = false;
        public int AC_PowerOhms = 0;
        public bool AC_LP30kHz = false;
        public bool AC_LP80kHz = false;
        public bool AC_HPLeft = false;
        public bool AC_HPRight = false;
        // DC Measurement
        public InputRangeDC DC_InputRange = InputRangeDC.Auto;
        public ScaleType DC_ScaleType = ScaleType.Lin;
        public bool DC_Ratio = false;
        // Distortion Measurement
        public InputRangeAC Dist_InputRange = InputRangeAC.Auto;
        public ScaleType Dist_ScaleType = ScaleType.Lin;
        public bool Dist_Ratio = false;
        public PostNotchGain Dist_PostNotchGain = PostNotchGain.Auto;
        public DetectorType Dist_DetectorType = DetectorType.RMS_Fast;
        public bool Dist_HoldNotchTuning = false;
        public bool Dist_LP30kHz = false;
        public bool Dist_LP80kHz = false;
        public bool Dist_HPLeft = false;
        public bool Dist_HPRight = false;
        // Distortion Level Measurement
        public InputRangeAC DistLevel_InputRange = InputRangeAC.Auto;
        public ScaleType DistLevel_ScaleType = ScaleType.Lin;
        public bool DistLevel_Ratio = false;
        public PostNotchGain DistLevel_PostNotchGain = PostNotchGain.Auto;
        public DetectorType DistLevel_DetectorType = DetectorType.RMS_Fast;
        public bool DistLevel_HoldNotchTuning = false;
        public bool DistLevel_LP30kHz = false;
        public bool DistLevel_LP80kHz = false;
        public bool DistLevel_HPLeft = false;
        public bool DistLevel_HPRight = false;
        // SNR Measurement
        public InputRangeAC SNR_InputRange = InputRangeAC.Auto;
        public ScaleType SNR_ScaleType = ScaleType.Lin;
        public bool SNR_Ratio = false;
        public PostNotchGain SNR_PostNotchGain = PostNotchGain.Auto;
        public DetectorType SNR_DetectorType = DetectorType.RMS_Fast;
        public bool SNR_HoldNotchTuning = false;
        public bool SNR_LP30kHz = false;
        public bool SNR_LP80kHz = false;
        public bool SNR_HPLeft = false;
        public bool SNR_HPRight = false;
        public SNRDelay SNR_Delay = SNRDelay.Auto;
        // SINAD Measurement
        public InputRangeAC SINAD_InputRange = InputRangeAC.Auto;
        public ScaleType SINAD_ScaleType = ScaleType.Lin;
        public bool SINAD_Ratio = false;
        public PostNotchGain SINAD_PostNotchGain = PostNotchGain.Auto;
        public DetectorType SINAD_DetectorType = DetectorType.RMS_Slow;
        public bool SINAD_HoldNotchTuning = false;
        public bool SINAD_LP30kHz = false;
        public bool SINAD_LP80kHz = false;
        public bool SINAD_HPLeft = false;
        public bool SINAD_HPRight = false;
        public SinadRange SINAD_Range = SinadRange.Range18dB;

        private InstrumentRetObject ExetuteMeasurement(MeasurementObject results, string command, Measurements measureType)
        {
            string readValue;
            InstrumentRetObject retObj = this.conn.Send(command);
            if (retObj.RetCode == InstrumentRetCode.OK)
            {
                retObj = this.conn.Read(ReadDisplay.Right.GetCommand() + Trigger.DelayedTrigger.GetCommand(), out readValue);
                if (retObj.RetCode == InstrumentRetCode.OK)
                {
                    // Process the string
                    results.Add(GetResultDescriptor((int)measureType), new MeasurementValue(Double.Parse(readValue, CultureInfo.InvariantCulture)));
                }
            }
            return retObj;
        }


        public override InstrumentRetObject Measure(MeasurementObject results)
        {
            InstrumentRetObject retObj = new InstrumentRetObject(InstrumentRetCode.OK);
            string command = "";
            MeasurementValue variableSource;
            // Setup generator
            if(this.analyzerType != AudioAnalyzerType.HB8903E)
            {
                command += GeneratorParam.Frequency.GetCommand();
                if (this.frequencySourceDescriptor == null)
                {
                    // Static value
                    command += GeneratorFrequency.ToString("0.000", CultureInfo.InvariantCulture);
                }
                else
                {
                    // Get from the results object
                    variableSource = results.Get(this.frequencySourceDescriptor);
                    command += variableSource.value.ToString("0.000", CultureInfo.InvariantCulture);
                }
                command += GeneratorFrequencyUnit.GetCommand();
                command += GeneratorParam.Amplitude.GetCommand();
                if (this.amplitudeSourceDescriptor == null)
                {
                    // Static value
                    command += GeneratorAmplitude.ToString("0.000", CultureInfo.InvariantCulture);
                    command += GeneratorAmplitudeUnit.GetCommand();
                }
                else
                {
                    // Get from the results object
                    variableSource = results.Get(this.amplitudeSourceDescriptor);
                    command += variableSource.value.ToString("0.000", CultureInfo.InvariantCulture);
                    switch(this.amplitudeSourceDescriptor.unit)
                    {
                        case MeasureUnit.volt:
                            command += Units.V.GetCommand();
                            break;
                        case MeasureUnit.dBm:
                            command += Units.dBm.GetCommand();
                            break;
                        default:
                            // Error
                            break;
                    }
                }
                // Send to the instrument
                retObj = conn.Send(command);
            }
            // Measure DC
            if (this.enabled_DC && retObj.RetCode == InstrumentRetCode.OK)
            {
                command = Measurements.DC_Level.GetCommand();
                command += DC_InputRange.GetCommand();
                command += DC_ScaleType.GetCommand();
                command += (DC_Ratio ? Ratio.On : Ratio.Off).GetCommand();
                retObj = ExetuteMeasurement(results, command, Measurements.DC_Level);
            }
            // Measure AC
            if (this.enabled_AC && retObj.RetCode == InstrumentRetCode.OK)
            {
                command = Measurements.AC_Level.GetCommand();
                command += AC_InputRange.GetCommand();
                command += AC_ScaleType.GetCommand();
                command += (AC_Ratio ? Ratio.On : Ratio.Off).GetCommand();
                command += AC_PostNotchGain.GetCommand();
                command += AC_DetectorType.GetCommand();
                command += (AC_HoldNotchTuning ? NotchTuning.Hold : NotchTuning.Automatic).GetCommand();
                command += (AC_LP30kHz ? LP_Filters.filter30kHz.GetCommand() : "");
                command += (AC_LP80kHz ? LP_Filters.filter80kHz.GetCommand() : "");
                command += (AC_LP30kHz || AC_LP80kHz ? "" : LP_Filters.Off.GetCommand());
                command += (AC_HPLeft ? HP_Filters.Left.GetCommand() : "");
                command += (AC_HPRight ? HP_Filters.Right.GetCommand() : "");
                command += (AC_HPLeft || AC_HPRight ? "" : HP_Filters.Off.GetCommand());
                if(AC_PowerDisplay)
                {
                    command += "19." + AC_PowerOhms.ToString() + "SP";
                }
                else
                {
                    // How to switch it off?
                }
                retObj = ExetuteMeasurement(results, command, Measurements.AC_Level);
            }
            // Measure Distortion
            if (this.enabled_Distortion && retObj.RetCode == InstrumentRetCode.OK)
            {
                command = Measurements.Distortion.GetCommand();
                command += Dist_InputRange.GetCommand();
                command += Dist_ScaleType.GetCommand();
                command += (Dist_Ratio ? Ratio.On : Ratio.Off).GetCommand();
                command += Dist_PostNotchGain.GetCommand();
                command += Dist_DetectorType.GetCommand();
                command += (Dist_HoldNotchTuning ? NotchTuning.Hold : NotchTuning.Automatic).GetCommand();
                command += (Dist_LP30kHz ? LP_Filters.filter30kHz.GetCommand() : "");
                command += (Dist_LP80kHz ? LP_Filters.filter80kHz.GetCommand() : "");
                command += (Dist_LP30kHz || Dist_LP80kHz ? "" : LP_Filters.Off.GetCommand());
                command += (Dist_HPLeft ? HP_Filters.Left.GetCommand() : "");
                command += (Dist_HPRight ? HP_Filters.Right.GetCommand() : "");
                command += (Dist_HPLeft || Dist_HPRight ? "" : HP_Filters.Off.GetCommand());
                retObj = ExetuteMeasurement(results, command, Measurements.Distortion);
            }
            // Measure Distortion Level
            if (this.enabled_DistortionLevel && retObj.RetCode == InstrumentRetCode.OK)
            {
                command = Measurements.Distortion_Level.GetCommand();
                command += DistLevel_InputRange.GetCommand();
                command += DistLevel_ScaleType.GetCommand();
                command += (DistLevel_Ratio ? Ratio.On : Ratio.Off).GetCommand();
                command += DistLevel_PostNotchGain.GetCommand();
                command += DistLevel_DetectorType.GetCommand();
                command += (DistLevel_HoldNotchTuning ? NotchTuning.Hold : NotchTuning.Automatic).GetCommand();
                command += (DistLevel_LP30kHz ? LP_Filters.filter30kHz.GetCommand() : "");
                command += (DistLevel_LP80kHz ? LP_Filters.filter80kHz.GetCommand() : "");
                command += (DistLevel_LP30kHz || DistLevel_LP80kHz ? "" : LP_Filters.Off.GetCommand());
                command += (DistLevel_HPLeft ? HP_Filters.Left.GetCommand() : "");
                command += (DistLevel_HPRight ? HP_Filters.Right.GetCommand() : "");
                command += (DistLevel_HPLeft || DistLevel_HPRight ? "" : HP_Filters.Off.GetCommand());
                retObj = ExetuteMeasurement(results, command, Measurements.Distortion_Level);
            }
            // Measure SNR
            if (this.enabled_SNR && retObj.RetCode == InstrumentRetCode.OK)
            {
                command = Measurements.SNR.GetCommand();
                command += SNR_InputRange.GetCommand();
                command += SNR_ScaleType.GetCommand();
                command += (SNR_Ratio ? Ratio.On : Ratio.Off).GetCommand();
                command += SNR_PostNotchGain.GetCommand();
                command += SNR_DetectorType.GetCommand();
                command += (SNR_HoldNotchTuning ? NotchTuning.Hold : NotchTuning.Automatic).GetCommand();
                command += (SNR_LP30kHz ? LP_Filters.filter30kHz.GetCommand() : "");
                command += (SNR_LP80kHz ? LP_Filters.filter80kHz.GetCommand() : "");
                command += (SNR_LP30kHz || SNR_LP80kHz ? "" : LP_Filters.Off.GetCommand());
                command += (SNR_HPLeft ? HP_Filters.Left.GetCommand() : "");
                command += (SNR_HPRight ? HP_Filters.Right.GetCommand() : "");
                command += (SNR_HPLeft || SNR_HPRight ? "" : HP_Filters.Off.GetCommand());
                command += SNR_Delay.GetCommand();
                retObj = ExetuteMeasurement(results, command, Measurements.SNR);
            }
            // Measure SINAD
            if (this.enabled_SINAD && retObj.RetCode == InstrumentRetCode.OK)
            {
                command = Measurements.SINAD.GetCommand();
                command += SINAD_InputRange.GetCommand();
                command += SINAD_ScaleType.GetCommand();
                command += (SINAD_Ratio ? Ratio.On : Ratio.Off).GetCommand();
                command += SINAD_PostNotchGain.GetCommand();
                command += SINAD_DetectorType.GetCommand();
                command += (SINAD_HoldNotchTuning ? NotchTuning.Hold : NotchTuning.Automatic).GetCommand();
                command += (SINAD_LP30kHz ? LP_Filters.filter30kHz.GetCommand() : "");
                command += (SINAD_LP80kHz ? LP_Filters.filter80kHz.GetCommand() : "");
                command += (SINAD_LP30kHz || SINAD_LP80kHz ? "" : LP_Filters.Off.GetCommand());
                command += (SINAD_HPLeft ? HP_Filters.Left.GetCommand() : "");
                command += (SINAD_HPRight ? HP_Filters.Right.GetCommand() : "");
                command += (SINAD_HPLeft || SINAD_HPRight ? "" : HP_Filters.Off.GetCommand());
                command += SINAD_Range.GetCommand();
                retObj = ExetuteMeasurement(results, command, Measurements.SINAD);
            }
            return retObj;
        }
        public override InstrumentRetObject Init()
        {
            return Setup(GPIB_Terminator.CRLF, new string[] { Trigger.Hold.GetCommand() });
        }
    }
}
