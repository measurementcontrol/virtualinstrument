﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using System.ComponentModel;
using SUF.Instrument;

namespace SUF.Instrument.HP_E1326B
{
    public static class ExtensionMethods
    {
        public static string GetRangePrefix<T>(this T e) where T : IConvertible
        {
            if (e is Enum)
            {
                Type type = e.GetType();
                Array values = System.Enum.GetValues(type);

                foreach (int val in values)
                {
                    if (val == e.ToInt32(CultureInfo.InvariantCulture))
                    {
                        var memInfo = type.GetMember(type.GetEnumName(val));
                        var prefixAttribute = memInfo[0]
                            .GetCustomAttributes(typeof(RangePrefixAttribute), false)
                            .FirstOrDefault() as RangePrefixAttribute;

                        if (prefixAttribute != null)
                        {
                            return prefixAttribute.Prefix;
                        }
                    }
                }
            }
            return null; // could also return string.Empty
        }

    }
    public class RangePrefixAttribute : Attribute
    {
        public RangePrefixAttribute(string prefix)
        {
            this.Prefix = prefix;
        }
        public string Prefix;
    }

    public enum LineFrequency
    {
        [Command("CAL:LFR 50"), Description("50 Hz")]
        Hz_50,
        [Command("CAL:LFR 60"), Description("60 Hz")]
        Hz_60
    }
    public enum AutoZero
    {
        [Command("CAL:ZERO:AUTO OFF")]
        Off = 0,
        [Command("CAL:ZERO:AUTO ON")]
        On = 1
    }

    public enum MeasurementFunction
    {
        [Command("VOLT:DC"), Description("DC Volts"), RangePrefix("VDC")]
        DC_Volts,
        [Command("VOLT:AC"), Description("AC Volts"), RangePrefix("VAC")]
        AC_Volts,
        [Command("RES"), Description("2-Wire Ohms"), RangePrefix("R")]
        Ohms_2wire,
        [Command("FRES"), Description("4-Wire Ohms"), RangePrefix("R")]
        Ohms_4wire,
        [Command("TEMP"), Description("Temperature"), RangePrefix("T")]
        Temp
    }

    public enum Range
    {
        [Command("AUTO"), Description("Auto")]
        VAC_Auto,
        [Command("0.0795"), Description("79.5 mV")]
        VAC_79mV5,
        [Command("0.63"), Description("630 mV")]
        VAC_63mV,
        [Command("5.09"), Description("5.09 V")]
        VAC_5V09,
        [Command("40.7"), Description("40.7 V")]
        VAC_40V7,
        [Command("300"), Description("300 V")]
        VAC_300V,

        [Command("AUTO"), Description("Auto")]
        VDC_Auto,
        [Command("0.113"), Description("113 mV")]
        VDC_113mV5,
        [Command("0.910"), Description("910 mV")]
        VDC_910mV,
        [Command("7.27"), Description("7.27 V")]
        VDC_7V27,
        [Command("58.1"), Description("58.1 V")]
        VAC_58V1,
        [Command("300"), Description("300 V")]
        VDC_300V,

        [Command("AUTO"), Description("Auto")]
        R_Auto,
        [Command("232"), Description("232 ohm")]
        R_232ohm,
        [Command("1861"), Description("1861 ohm")]
        R_1861ohm,
        [Command("14894"), Description("14894 ohm")]
        R_14894ohm,
        [Command("119156"), Description("119156 ohm")]
        R_119156ohm,
        [Command("1048576"), Description("1048576 ohm")]
        R_1048576ohm
    }

    public enum TempTransudcerType
    {
        [Command("TC"), Description("Thermocouple"), RangePrefix("TC")]
        Thermocouple,
        [Command("THER"), Description("Thermistor 2-Wire"), RangePrefix("T")]
        T2,
        [Command("FTH"), Description("Thermistor 4-Wire"), RangePrefix("T")]
        T4,
        [Command("RTD"), Description("RTD 2-Wire"), RangePrefix("R")]
        Ohms_4wire,
        [Command("FRTD"), Description("RTD 4-Wire"), RangePrefix("R")]
        Temp
    }

    public enum TempSensorType
    {
        [Command("B"), Description("B")]
        TC_B,
        [Command("E"), Description("E")]
        TC_E,
        [Command("J"), Description("J")]
        TC_J,
        [Command("K"), Description("K")]
        TC_K,
        [Command("N14"), Description("N14")]
        TC_N14,
        [Command("N28"), Description("N28")]
        TC_N28,
        [Command("R"), Description("R")]
        TC_R,
        [Command("S"), Description("S")]
        TC_S,
        [Command("T"), Description("T")]
        TC_T,

        [Command("2252"), Description("2252 ohm")]
        T_2252,
        [Command("5000"), Description("5 kohm")]
        T_5k,
        [Command("10000"), Description("10 kohm")]
        T_10k,

        [Command("85"), Description("Type 85")]
        R_85,
        [Command("92"), Description("Type 92")]
        R92
    }

    public enum ResolutionPLC
    {
        [Command("NPLC 0.0005"), Description("0.0005")]
        PLC_m5,
        [Command("NPLC 0.005"), Description("0.005")]
        PLC_5m,
        [Command("NPLC 0.125"), Description("0.125")]
        PLC_125m,
        [Command("NPLC 1"), Description("1")]
        PLC_1,
        [Command("NPLC 16"), Description("16")]
        PLC_16
    }

    /// <summary>
    /// Copied
    /// </summary>

    public enum Trigger
    {
        [Command("T1")]
        Internal,
        [Command("T2")]
        External,
        [Command("T3")]
        Single,
        [Command("T4")]
        Hold
    }



}
