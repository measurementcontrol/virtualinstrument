﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SUF.Instrument;
using SUF.Instrument.Serial;

namespace SUF.Instrument.AvrGPIB
{
    public partial class FormAvrGPIBConnectionSettings : ConnectionSettingsFormBase
    {
        public FormAvrGPIBConnectionSettings()
        {
            InitializeComponent();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            string ifName = "unknown";
            if (this.cbxInterfaces.SelectedItem is AvrGPIBInterface)
            {
                ((AvrGPIBConnection)base.connection).SAD = this.chkDevSADEnable.Checked ? Convert.ToInt32(this.numDevSAD.Value + 96) : 0;
                ((AvrGPIBConnection)base.connection).PAD = Convert.ToInt32(this.numDevPAD.Value);
                ((AvrGPIBConnection)base.connection).GPIBInterface = (AvrGPIBInterface)this.cbxInterfaces.SelectedItem;
                if (this.tbxName.Text == "")
                {
                    if(((AvrGPIBConnection)base.connection).GPIBInterface.Name != null && ((AvrGPIBConnection)base.connection).GPIBInterface.Name != "")
                    {
                        ifName = ((AvrGPIBConnection)base.connection).GPIBInterface.Name;
                    }
                    ((AvrGPIBConnection)base.connection).Name = ifName + ":" + ((AvrGPIBConnection)base.connection).PAD.ToString() + (((AvrGPIBConnection)base.connection).SAD > 0 ? ":" + ((AvrGPIBConnection)base.connection).SAD.ToString() : "");
                }
                else
                {
                    ((AvrGPIBConnection)base.connection).Name = this.tbxName.Text;
                }
                base.IsSaved = true;
            }
            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGPIBInterface_Click(object sender, EventArgs e)
        {
            FormAvrGPIBInterfaceList AvrGPIBInterfaceListDialog = new FormAvrGPIBInterfaceList();
            if(base.connection is AvrGPIBConnection)
            {
                AvrGPIBInterfaceListDialog.GPIBInterface = ((AvrGPIBConnection)base.connection).GPIBInterface;
            }
            AvrGPIBInterfaceListDialog.ShowDialog();
            if(AvrGPIBInterfaceListDialog.IsSaved)
            {
                if(base.connection is AvrGPIBConnection)
                {
                    ((AvrGPIBConnection)base.connection).GPIBInterface = AvrGPIBInterfaceListDialog.GPIBInterface;
                }
            }
            AvrGPIBInterfaceListDialog.Dispose();
            /*
            AvrGPIBInterfaceSettings GPIBInterface;
            GPIBInterface = this.cbxInterfaces.SelectedItem.GetType() == typeof(AvrGPIBInterfaceSettings) ? (AvrGPIBInterfaceSettings)this.cbxInterfaces.SelectedItem : null;
            FormAvrGPIBConfig formAvrGPIBConfig = new FormAvrGPIBConfig(GPIBInterface);
            formAvrGPIBConfig.ShowDialog();
            if(formAvrGPIBConfig.IsSaved)
            {
                ((AvrGPIBConnectionSettings)this.Settings).GPIBInterface = formAvrGPIBConfig.PhysicalInterface;
                UpdateInterfaces();
            }
            formAvrGPIBConfig.Dispose();
            */
            UpdateInterfaces();
        }
        public void UpdateInterfaces()
        {
            this.cbxInterfaces.Items.Clear();
            // this.cbxInterfaces.SelectedIndex = this.cbxInterfaces.Items.Add("<...NEW...>");
            int ItemID;
            foreach (AvrGPIBInterface GPIBInterface in AvrGPIBInterfaceCollection.GetAvrGPIBInterfaceCollection.GPIBInterfaces)
            {
                ItemID = this.cbxInterfaces.Items.Add(GPIBInterface);
                if(GPIBInterface == ((AvrGPIBConnection)base.connection).GPIBInterface)
                {
                    this.cbxInterfaces.SelectedIndex = ItemID;
                }
            }
        }

        private void chkDevSADEnable_CheckedChanged(object sender, EventArgs e)
        {
            this.numDevSAD.Enabled = this.chkDevSADEnable.Checked;
        }

        private void FormAvrGPIBConnectionSettings_Load(object sender, EventArgs e)
        {
            if(!(base.connection is AvrGPIBConnection))
            {
                base.connection = new AvrGPIBConnection();
            }
            UpdateInterfaces();

            this.chkDevSADEnable.Checked = ((AvrGPIBConnection)base.connection).SAD > 0;
            this.numDevSAD.Enabled = this.chkDevSADEnable.Checked;
            if (((AvrGPIBConnection)base.connection).SAD > 0)
            {
                this.numDevSAD.Value = ((AvrGPIBConnection)base.connection).SAD - 96;
            }
            this.numDevPAD.Value = ((AvrGPIBConnection)base.connection).PAD;
            tbxName.Text = ((AvrGPIBConnection)base.connection).Name;
        }
    }
}
