﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO.Ports;
using System.Collections;

namespace SUF.Instrument.Serial
{
    public sealed class SerialCollection : IEnumerable<SerialPort>
    {
        private SerialCollection()
        {
            this._ports = new Dictionary<string,SerialPort>();
        }
        private static readonly Lazy<SerialCollection> lazy = new Lazy<SerialCollection>(() => new SerialCollection());
        public static SerialCollection GetSerialCollection
        {
            get
            {
                return lazy.Value;
            }
        }
        private Dictionary<string,SerialPort> _ports;
        public string Add(SerialPort port)
        {
            string retvalue = null;
            if (port.PortName != "" && port.PortName != null)
            {
                if (!this._ports.ContainsKey(port.PortName))
                {
                    this._ports.Add(port.PortName, port);
                    retvalue = port.PortName;
                }
            }
            return retvalue;
        }
        public void Remove(string id)
        {
            if (this._ports.ContainsKey(id))
            {
                // if the port open, must be closed before removal
                if(this._ports[id].IsOpen)
                {
                    this._ports[id].Close();
                }
                this._ports.Remove(id);
            }
        }

        public bool Contains(string PortName)
        {
            return this._ports.ContainsKey(PortName);
        }

        public IEnumerator<SerialPort> GetEnumerator()
        {
            return this._ports.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._ports.Values.GetEnumerator();
        }
    }
}
