﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace SUF.Instrument.Serial
{
    /// <summary>
    /// Collection of the connection settings
    /// </summary>
    public sealed class SerialPortCollection
    {
        private SerialPortCollection() { }
        private static readonly Lazy<SerialPortCollection> lazy = new Lazy<SerialPortCollection>(() => new SerialPortCollection());
        public static SerialPortCollection GetSerialPortCollection
        {
            get
            {
                return lazy.Value;
            }
        }
        public BindingList<SerialPortEx> SerialPorts = new BindingList<SerialPortEx>();
    }
}
