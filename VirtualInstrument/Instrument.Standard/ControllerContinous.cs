﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SUF.Instrument;

namespace SUF.Instrument.Standard
{
    [PluginDescriptor("Continous Controller")]
    public class ControllerContinous : Controller
    {
        private CancellationTokenSource ControllerWorker_ts;
        private CancellationToken ControllerWorker_ct;
        // private TaskCompletionSource<int> ControllerWorker_tcs = new TaskCompletionSource<int>();
        // private Task<int> ControllerWorkerTask;
        public ControllerContinous() { }
        public override void Start()
        {
            this.State = ControllerState.Running;
            // initialize the devices (instrument, filter, target)
            InitDevices();
            ControllerWorker_ts = new CancellationTokenSource();
            ControllerWorker_ct = ControllerWorker_ts.Token;
            // ControllerWorkerTask = ControllerWorker_tcs.Task;
            Task.Run((Action)ControllerWorker, ControllerWorker_ct);
        }
        public override void Stop()
        {
            ControllerWorker_ts.Cancel();
            // Wailt for stop
            // int dummyResult = ControllerWorkerTask.Result;
        }
        private void ControllerWorker()
        {
            MeasurementObject results;
            int SequenceNumber = 1;
            while (!ControllerWorker_ct.IsCancellationRequested)
            {
                results = new MeasurementObject();
                results.TimeStamp = DateTime.Now;
                results.SequenceNumber = SequenceNumber++;
                RunMeasurementCycle(results);
            }
            this.State = ControllerState.Stopped;
            // ControllerWorker_tcs.SetResult(0);
        }
    }
}
