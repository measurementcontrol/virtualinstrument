﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace SUF.Instrument.Standard
{
    [PluginDescriptor("Chart Display")]
    [PluginSettingsClass("SUF.Instrument.Standard.FormTargetChartSettings")]
    public partial class FormTargetChart : Form, ITarget, IResultConsumer
    {
        public ResultDescriptor AxisXresultDescriptor;
        public bool AxisXUseSequence = true;
        public bool AxisXLogarithmic = false;
        public List<ChartSeriesDescriptor> ChartSeriesDescriptors = new List<ChartSeriesDescriptor>();
        // Temporarly - Multiple Y Axis handling required
        private bool MinMaxSet = false;
        private double Min = 0;
        private double Max = 0;

        private double XMin = 1;
        private bool XMinSet = false;

        public FormTargetChart()
        {
            InitializeComponent();
        }

        public string GetName()
        {
            return this.Text;
        }

        public void Init()
        {
            this.chrtResults.Series.Clear();
            foreach (ChartSeriesDescriptor descriptor in this.ChartSeriesDescriptors)
            {
                if(this.AxisXLogarithmic)
                {
                    descriptor.ChartSeries.IsXValueIndexed = false;
                }
                this.chrtResults.Series.Add(descriptor.ChartSeries);
            }

            this.chrtResults.Invalidate();
            if (!this.Visible)
            {
                this.Show();
            }
        }

        public void ProcessResult(MeasurementObject results)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<MeasurementObject>(ProcessResult), new object[] { results });
                return;
            }
            foreach (ChartSeriesDescriptor descriptor in this.ChartSeriesDescriptors)
            {
                if(!MinMaxSet)
                {
                    Min = results.Get(descriptor.SeriesResultDescriptor).value;
                    Max = results.Get(descriptor.SeriesResultDescriptor).value;
                    MinMaxSet = true;
                }
                else
                {
                    if(Min > results.Get(descriptor.SeriesResultDescriptor).value)
                    {
                        Min = results.Get(descriptor.SeriesResultDescriptor).value;
                    }
                    if(Max < results.Get(descriptor.SeriesResultDescriptor).value)
                    {
                        Max = results.Get(descriptor.SeriesResultDescriptor).value;
                    }
                }
                if (Min != Max)
                {
                    this.chrtResults.ChartAreas[0].AxisY.Minimum = Min;
                    this.chrtResults.ChartAreas[0].AxisY.Maximum = Max;
                }
                descriptor.ChartSeries.Points.AddXY(AxisXUseSequence ? results.SequenceNumber : results.Get(AxisXresultDescriptor).value, results.Get(descriptor.SeriesResultDescriptor).value);

            }
            if (!XMinSet)
            {
                XMinSet = true;
                XMin = results.Get(AxisXresultDescriptor).value;
                this.chrtResults.ChartAreas[0].AxisX.Minimum = XMin;
            }
            else
            {
                if (results.Get(AxisXresultDescriptor).value < XMin)
                {
                    XMin = results.Get(AxisXresultDescriptor).value;
                    this.chrtResults.ChartAreas[0].AxisX.Minimum = XMin;
                }
            }
            if (this.AxisXLogarithmic != this.chrtResults.ChartAreas[0].AxisX.IsLogarithmic)
            {
                if (this.AxisXLogarithmic)
                {
                    // this.chrtResults.ChartAreas[0].AxisX.Minimum = 1;
                    this.chrtResults.ChartAreas[0].AxisX.IsLogarithmic = true;
                    this.chrtResults.ChartAreas[0].AxisX.MinorGrid.Interval = 1;
                    this.chrtResults.ChartAreas[0].AxisX.MinorGrid.Enabled = true;
                    this.chrtResults.ChartAreas[0].AxisX.LogarithmBase = 10;
                }
                else
                {
                    this.chrtResults.ChartAreas[0].AxisX.IsLogarithmic = false;
                    this.chrtResults.ChartAreas[0].AxisX.MinorGrid.Enabled = false;
                }
            }
            this.chrtResults.Invalidate();
        }

        public void SetName(string name)
        {
            this.Text = name;
        }

    }
    public class ChartSeriesDescriptor
    {
        public ResultDescriptor SeriesResultDescriptor;
        public Series ChartSeries;
        public bool IsLogarithmic = false;
        public ChartSeriesDescriptor() { }
    }
}
