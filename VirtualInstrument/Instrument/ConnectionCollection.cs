﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    /// <summary>
    /// Collection of the connection settings
    /// </summary>
    public sealed class ConnectionCollection
    {
        private ConnectionCollection() {}
        private static readonly Lazy<ConnectionCollection> lazy = new Lazy<ConnectionCollection>(() => new ConnectionCollection());
        public static ConnectionCollection GetConnectionCollection
        {
            get
            {
                return lazy.Value;
            }
        }
        public BindingList<Connection> Connections = new BindingList<Connection>();
    }

    /*
    public sealed class ConnectionCollection : IEnumerable<Connection>
    {
        private ConnectionCollection()
        {
            this._connection = new Dictionary<int, Connection>();
        }
        private static readonly Lazy<ConnectionCollection> lazy = new Lazy<ConnectionCollection>(() => new ConnectionCollection());
        public static ConnectionCollection GetConnectionCollection
        {
            get
            {
                return lazy.Value;
            }
        }
        private int _currentId = 0;
        private Dictionary<int, Connection> _connection;
        public int Add(Connection connSettings)
        {
            int id = GetIdByConnection(connSettings);
            if (id == -1)
            {
                id = this._currentId++;
                this._connection.Add(id, connSettings);
            }
            return id;
        }
        public void Remove(int id)
        {
            if (this._connection.ContainsKey(id))
            {
                this._connection.Remove(id);
            }
        }
        public int GetIdByConnection(Connection connection)
        {

            foreach(int key in this._connection.Keys)
            {
                if(this._connection[key] == connection || this._connection[key].Equals(connection))
                {
                    return key;
                }
            }
            return -1;
        }

        public IEnumerator<Connection> GetEnumerator()
        {
            return this._connection.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._connection.Values.GetEnumerator();
        }
    }
     */
}
