﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    /// <summary>
    /// Base type of all Filter objects
    /// </summary>
    public class Filter : ResultProvider
    {
        public virtual void FilterResult(MeasurementObject results)
        { }
        public virtual void Init()
        { }
    }
}
