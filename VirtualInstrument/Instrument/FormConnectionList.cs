﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SUF.Instrument;

namespace SUF.Instrument
{
    public partial class FormConnectionList : Form
    {
        public bool IsSaved = false;
        public Connection connection;
        public FormConnectionList()
        {
            InitializeComponent();

            this.lbxConnection.DisplayMember = "Name";
            this.lbxConnection.ValueMember = "Value";
            this.lbxConnection.DataSource = ConnectionCollection.GetConnectionCollection.Connections;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            FormConnection connDialog = new FormConnection();
            connDialog.ShowDialog();
            if(connDialog.IsSaved)
            {
                ConnectionCollection.GetConnectionCollection.Connections.Add(connDialog.connection);
            }
            connDialog.Dispose();
        }

        private void lbxConnection_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.lbxConnection.IndexFromPoint(e.Location) >= 0)
            {
                EditConnection((Connection)this.lbxConnection.Items[this.lbxConnection.IndexFromPoint(e.Location)]);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditConnection((Connection)this.lbxConnection.SelectedItem);
        }

        private void EditConnection(Connection connection)
        {
            ConnectionSettingsFormBase settingsForm;
            settingsForm = ConnectionTypeCollection.GetConnectionTypeCollection.GetConnectionPlugin(connection.GetType()).GetSettingsForm();
            settingsForm.connection = connection;
            settingsForm.ShowDialog();
            if(settingsForm.IsSaved)
            {
                ConnectionCollection.GetConnectionCollection.Connections.ResetBindings();
            }
            settingsForm.Dispose();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.connection = (Connection)this.lbxConnection.SelectedItem;
            this.IsSaved = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {

        }
        public static void SetupConnection(ref Connection conn)
        {
            FormConnectionList connectionForm = new FormConnectionList();
            if (conn != null)
            {
                connectionForm.connection = conn;
            }
            connectionForm.ShowDialog();
            if (connectionForm.IsSaved)
            {
                conn = connectionForm.connection;
            }
            connectionForm.Dispose();
        }

        private void FormConnectionList_Load(object sender, EventArgs e)
        {
            if(this.connection != null)
            {
                foreach(Connection conn in this.lbxConnection.Items)
                {
                    if(conn == this.connection)
                    {
                        this.lbxConnection.SelectedItem = this.connection;
                        break;
                    }
                }
            }
        }
    }
}
