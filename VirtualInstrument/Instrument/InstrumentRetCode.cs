﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    public enum InstrumentRetCode
    {
        OK = 0,
        Busy = 1,
        AddressConflict = 2,
        NotImplemented = 3,
        MissingPHY = 4,
        PHYOpenError = 5,
        PHYAlreadyOpen = 6,
        PHYTimeOut = 7,
        PHYCommunicationError = 8,
        PHYNotOpen = 9,
        PHYCloseError = 10,
        PHYAlreadyClosed = 11
    }
    public class InstrumentRetObject
    {
        public InstrumentRetObject() { }
        public InstrumentRetObject(InstrumentRetCode code)
        {
            this.RetCode = code;
        }
        public InstrumentRetCode RetCode;
        public object[] RetInfo;
    }
}
