﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SUF.Instrument;

namespace SUF.Instrument.Keithley_199
{
    public partial class FormKeithley_199InstrumentSettings : InstrumentSettingsFormBase
    {
        public FormKeithley_199InstrumentSettings()
        {
            InitializeComponent();
            _funcPrefixLookup = new Dictionary<MeasurementFunction, string>();
            _funcPrefixLookup.Add(MeasurementFunction.AC_Current, "ACI");
            _funcPrefixLookup.Add(MeasurementFunction.AC_Volts, "ACV");
            _funcPrefixLookup.Add(MeasurementFunction.DC_Current, "DCI");
            _funcPrefixLookup.Add(MeasurementFunction.DC_Volts, "DCV");
            _funcPrefixLookup.Add(MeasurementFunction.Ohms, "R");
            _funcPrefixLookup.Add(MeasurementFunction.ACV_dB, "ACVdB");
            _funcPrefixLookup.Add(MeasurementFunction.ACA_dB, "ACAdB");
        }
        private Dictionary<MeasurementFunction, string> _funcPrefixLookup;

        private void cbxFunction_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateRanges();
        }
        private void UpdateRanges()
        {
            int ItemID;
            this.cbxRange.Items.Clear();
            this.cbxRange.SelectedIndex = -1;
            this.cbxRange.Text = ""; // change to a new auto???
            foreach (Range range in Enum.GetValues(typeof(Range)))
            {
                if (range.ToString().StartsWith(_funcPrefixLookup[(MeasurementFunction)cbxFunction.SelectedItem] + "_"))
                {
                    ItemID = this.cbxRange.Items.Add(range);
                    if ((Range)this.cbxRange.Items[ItemID] == ((InstrumentKeithley_199)this.instrument).range)
                    {
                        this.cbxRange.SelectedIndex = ItemID;
                    }
                }
            }
            if(this.cbxRange.SelectedIndex == -1)
            {
                foreach(Range range in this.cbxRange.Items)
                {
                    if(range.ToString().EndsWith("_Auto"))
                    {
                        this.cbxRange.SelectedItem = range;
                    }
                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            // fill the instrument
            ((InstrumentKeithley_199)this.instrument).func = (MeasurementFunction)this.cbxFunction.SelectedItem;
            ((InstrumentKeithley_199)this.instrument).range = (Range)this.cbxRange.SelectedItem;
            ((InstrumentKeithley_199)this.instrument).precision = (Resolution)this.cbxPrecision.SelectedItem;
            base.IsSaved = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormHP3478AInstrumentSettings_Load(object sender, EventArgs e)
        {
            if (base.instrument == null)
            {
                base.instrument = new InstrumentKeithley_199();
            }
            /*
            this.cbxFunction.Items.Clear();
            foreach (MeasurementFunction func in Enum.GetValues(typeof(MeasurementFunction)))
            {
                ItemID = this.cbxFunction.Items.Add(func);
                if ((MeasurementFunction)this.cbxFunction.Items[ItemID] == ((InstrumentHP3478A)this.instrument).func)
                {
                    this.cbxFunction.SelectedIndex = ItemID;
                }
            }
            */
            UpdateEnumComboBox<MeasurementFunction>(this.cbxFunction, ((InstrumentKeithley_199)this.instrument).func);
            UpdateRanges();
            UpdateEnumComboBox<Resolution>(this.cbxPrecision, ((InstrumentKeithley_199)this.instrument).precision);
        }

        private void btnConnection_Click(object sender, EventArgs e)
        {
            FormConnectionList.SetupConnection(ref this.instrument.conn);
        }

        private void CbxFunction_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = ((MeasurementFunction)e.ListItem).GetDescription();
        }

        private void CbxRange_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = ((Range)e.ListItem).GetDescription();
        }

        private void CbxResolution_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = ((Resolution)e.ListItem).GetDescription();
        }

    }
}
