﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using SUF.Instrument;

namespace SUF.Instrument.Keithley_199
{
    [PluginDescriptor("Keithley 199 Multimeter")]
    [PluginSettingsClass("SUF.Instrument.Keithley_199.FormKeithley_199InstrumentSettings")]
    public class InstrumentKeithley_199 : Instrument
    {
        public ResultDescriptor resultDescriptor;
        public InstrumentKeithley_199()
        {
            base.sourceDescriptors = new List<ResultDescriptor>();
            resultDescriptor = new ResultDescriptor();
            resultDescriptor.SourceGuid = base.InstanceID;
            resultDescriptor.ChannelID = 0;
            base.sourceDescriptors.Add(resultDescriptor);
            this.func = MeasurementFunction.DC_Volts;
        }

        // Parameters set by the settingsform
        public MeasurementFunction func
        {
            get
            {
                return this._func;
            }
            set
            {
                this._func = value;
                this.resultDescriptor.ChannelName = value.ToString();
                this.resultDescriptor.unit = value.GetMeasureUnit();
            }
        }
        private MeasurementFunction _func;
        public Range range = Range.DCV_Auto;
        public Resolution precision = Resolution.Digit_55;

        public int ResultID;
        public override InstrumentRetObject Measure(MeasurementObject results)
        {
            string result;
            MeasurementValue value;
            InstrumentRetObject retObj = conn.Read("X", out result);
            if (retObj.RetCode == InstrumentRetCode.OK)
            {
                // Extract result, check overload condition
                if(result.StartsWith("N") || result.StartsWith("Z"))
                {
                    // normal operation, if it start with "O" means overload
                    value = new MeasurementValue(Convert.ToDouble(result.Substring(4), CultureInfo.InvariantCulture), MetricPrefix.none);
                }
                else
                {
                    value = new MeasurementValue(0);
                    // Handover overflow flag somehow. Question at MeasurementValue object, how to handle
                }
                // The result translation should be corrected by actual measurements
                results.Add(this.resultDescriptor, value);
            }
            return retObj;
        }
        public override InstrumentRetObject Init()
        {
            // function, range, precision, G0 - Reading with prefix, Z0 - Zeroing off, P0 - filter off, Trigger on X character, X - execute settings
            // Note: Zeroing - The Keithley 199 has only manual zeroing capability. It is a question, how can be this handled for the automated
            //       measurement process of the Virtual Instrument software. Now it is just switched off.
            //       Filter - The Keithley 199 has a "filtering" capability. According to my understanding it is a windowed measurement averaging.
            //       It is switched off right now, as using it with the polling method (Single triggers) can slow down the measurement process
            //       extremely
            return Setup(GPIB_Terminator.EOI, new string[] { func.GetCommand() + range.GetCommand() + precision.GetCommand() + "G0Z0P0" + Trigger.X_Single.GetCommand() + "X" });
        }
    }
}
