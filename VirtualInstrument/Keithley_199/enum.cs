﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using System.ComponentModel;
using SUF.Instrument;

namespace SUF.Instrument.Keithley_199
{
    public enum MeasurementFunction
    {
        [Command("F0"), Description("DC Volts"), MeasureUnit(MeasureUnit.volt)]
        DC_Volts,
        [Command("F1"), Description("AC Volts"), MeasureUnit(MeasureUnit.volt)]
        AC_Volts,
        [Command("F2"), Description("Ohms"), MeasureUnit(MeasureUnit.ohm)]
        Ohms,
        [Command("F3"), Description("DC Current"), MeasureUnit(MeasureUnit.ampere)]
        DC_Current,
        [Command("F4"), Description("AC Current"), MeasureUnit(MeasureUnit.ampere)]
        AC_Current,
        [Command("F5"), Description("AC Volts (dB)"), MeasureUnit(MeasureUnit.decibel)]
        ACV_dB,
        [Command("F6"), Description("AC Current (dB)"), MeasureUnit(MeasureUnit.decibel)]
        ACA_dB
    }
    public enum Range
    {
        [Command("R0"), Description("Auto")]
        DCV_Auto,
        [Command("R0"), Description("Auto")]
        ACV_Auto,
        [Command("R0"), Description("Auto")]
        DCI_Auto,
        [Command("R0"), Description("Auto")]
        ACI_Auto,
        [Command("R0"), Description("Auto")]
        R_Auto,
        [Command("R0"), Description("Auto")]
        ACVdB_Auto,
        [Command("R0"), Description("Auto")]
        ACAdB_Auto,
        [Command("R1"), Description("300mV")]
        DCV_300mV,
        [Command("R1"), Description("300mV")]
        ACV_300mV,
        [Command("R1"), Description("30mA")]
        DCI_30mA,
        [Command("R1"), Description("30mA")]
        ACI_30mA,
        [Command("R1"), Description("300ohm")]
        R_300ohm,
        [Command("R2"), Description("3V")]
        DCV_3V,
        [Command("R2"), Description("3V")]
        ACV_3V,
        [Command("R2"), Description("3Kohm")]
        R_3kohm,
        [Command("R3"), Description("30V")]
        DCV_30V,
        [Command("R3"), Description("30V")]
        ACV_30V,
        [Command("R3"), Description("30Kohm")]
        R_30kohm,
        [Command("R4"), Description("300V")]
        DCV_300V,
        [Command("R4"), Description("300V")]
        ACV_300V,
        [Command("R4"), Description("300Kohm")]
        R_300kohm,
        [Command("R5"), Description("3Mohm")]
        R_3Mohm,
        [Command("R6"), Description("30Mohm")]
        R_30Mohm,
        [Command("R7"), Description("300Mohm")]
        R_300Mohm
    }

    public enum Resolution
    {
        [Command("S0"), Description("4.5")]
        Digit_45,
        [Command("S1"), Description("5.5")]
        Digit_55,
    }

    public enum Trigger
    {
        [Command("T0")]
        Talk_Continous,
        [Command("T1")]
        Talk_Single,
        [Command("T2")]
        Get_Continous,
        [Command("T3")]
        Get_Single,
        [Command("T4")]
        X_Continous,
        [Command("T5")]
        X_Single,
        [Command("T6")]
        Ext_Continous,
        [Command("T7")]
        Ext_Single
    }
}