Creating new Instrument
1. Add a new project - Type: Class Library (.NET Framework)
   Target Framework: .NET Framework 4.7.2
2. Switch the namespace to SUF.Instrument.<project name> in the project properties and in the class cs file
3. Add reference to the Instrument project
4. Optional (if the instrument has settings GUI) add System.Drawing and System.Windows.Forms reference
5. The new instrument class should inherit SUF.Instrument.Instrument
6. Add PluginDescriptor attribute with the value of the Instrument display name

Add Settings window
1. Add new item - Form (Windows Forms)
2. The settings form should inherit SUF.Instrument.Instrument.InstrumentSettingsFormBase instead of Form
3. Add PluginSettingsClass attribute with the Form full class name (namespace + Form Class name)
(What if I add this into the form class source ???)
